package org.bitbucket.arvade.csgoapi.service;

import org.bitbucket.arvade.csgoapi.model.CsgoEvent;
import org.bitbucket.arvade.csgoapi.model.RawCsgoEventResource;

public interface CsgoEventConverter {

    RawCsgoEventResource convert(CsgoEvent csgoEvent);

}
