package org.bitbucket.arvade.csgoapi.service.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.csgoapi.common.ImageScaler;
import org.bitbucket.arvade.csgoapi.model.CsgoEvent;
import org.bitbucket.arvade.csgoapi.model.RawCsgoEventResource;
import org.bitbucket.arvade.csgoapi.service.CsgoEventConverter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;

public class CsgoEventConverterImpl implements CsgoEventConverter {

    private ImageScaler imageScaler;

    @Inject
    public CsgoEventConverterImpl(ImageScaler imageScaler) {
        this.imageScaler = imageScaler;
    }

    @Override
    public RawCsgoEventResource convert(CsgoEvent csgoEvent) {
        RawCsgoEventResource rawCsgoEventResource = new RawCsgoEventResource();
        mapKeys(csgoEvent, rawCsgoEventResource);
        mapImage(csgoEvent, rawCsgoEventResource);
        //119 - w
        //115 - s
        //97 -  a
        //100 - d
        //    - Ctrl ?
        //32 - space
        return rawCsgoEventResource;
    }

    private void mapKeys(CsgoEvent csgoEvent, RawCsgoEventResource rawCsgoEventResource) {
        Set<Character> pressedKeys = csgoEvent.getPressedKeys();

        rawCsgoEventResource.setWPressed(pressedKeys.contains('w') || pressedKeys.contains('W'));
        rawCsgoEventResource.setSPressed(pressedKeys.contains('s') || pressedKeys.contains('S'));
        rawCsgoEventResource.setAPressed(pressedKeys.contains('a') || pressedKeys.contains('A'));
        rawCsgoEventResource.setDPressed(pressedKeys.contains('d') || pressedKeys.contains('D'));

        //TODO: Sprawdzic ich keyCode
        rawCsgoEventResource.setSpacePressed(false);
        rawCsgoEventResource.setCtrlPressed(false);
    }

    private void mapImage(CsgoEvent csgoEvent, RawCsgoEventResource rawCsgoEventResource) {
        BufferedImage bufferedImage = csgoEvent.getBufferedImage();
        BufferedImage scaledImage = imageScaler.scaleImage(bufferedImage, 160, 120);
        byte[] scaledImageAsBytes = toByteArray(scaledImage);
        rawCsgoEventResource.setImage(scaledImageAsBytes);
    }

    private byte[] toByteArray(BufferedImage image) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", out);
            return out.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


