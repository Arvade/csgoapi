package org.bitbucket.arvade.csgoapi.service.impl;

import com.google.inject.Inject;
import lombok.extern.log4j.Log4j;
import org.bitbucket.arvade.csgoapi.model.CsgoEvent;
import org.bitbucket.arvade.csgoapi.model.EventConsumerConfiguration;
import org.bitbucket.arvade.csgoapi.model.RawCsgoEventResource;
import org.bitbucket.arvade.csgoapi.repository.CsgoEventRepository;
import org.bitbucket.arvade.csgoapi.service.CsgoEventConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

@Log4j
public class EventConsumer implements Runnable {

    private final BlockingQueue<CsgoEvent> queue;
    private EventConsumerConfiguration config;
    private CsgoEventRepository csgoEventRepository;
    private CsgoEventConverter csgoEventConverter;

    @Inject
    public EventConsumer(BlockingQueue<CsgoEvent> queue,
                         EventConsumerConfiguration eventConsumerConfiguration,
                         CsgoEventRepository csgoEventRepository,
                         CsgoEventConverter csgoEventConverter) {
        this.queue = queue;
        this.config = eventConsumerConfiguration;
        this.csgoEventRepository = csgoEventRepository;
        this.csgoEventConverter = csgoEventConverter;
    }

    @Override
    public void run() {
        while (true) {
            try {
                List<RawCsgoEventResource> rawCsgoEventResources = collectEvents();
                save(rawCsgoEventResources);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private List<RawCsgoEventResource> collectEvents() throws InterruptedException {
        List<RawCsgoEventResource> rawCsgoEventResources = new ArrayList<>();

        for (int i = 0; i < config.getSizeOfArrayToStoreEntities(); i++) {
            CsgoEvent csgoEvent = queue.take();
            RawCsgoEventResource convert = csgoEventConverter.convert(csgoEvent);
            rawCsgoEventResources.add(convert);
        }

        return rawCsgoEventResources;
    }

    private void save(List<RawCsgoEventResource> rawCsgoEventResources) {
        long start = System.currentTimeMillis();
        csgoEventRepository.save(rawCsgoEventResources);
        long stop = System.currentTimeMillis();
        long delta = stop - start;
        log.info("Flushing took " + delta + " ms");
    }
}