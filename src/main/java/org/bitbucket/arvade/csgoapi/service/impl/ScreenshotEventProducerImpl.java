package org.bitbucket.arvade.csgoapi.service.impl;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.bitbucket.arvade.csgoapi.listener.PressedKeysProvider;
import org.bitbucket.arvade.csgoapi.model.CsgoEvent;
import org.bitbucket.arvade.csgoapi.model.RawCsgoEventResource;
import org.bitbucket.arvade.csgoapi.service.CsgoEventConverter;
import org.bitbucket.arvade.csgoapi.service.ScreenshotEventProducer;
import org.bitbucket.arvade.csgoapi.util.ScreenResolutionHelper;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

@Log4j
public class ScreenshotEventProducerImpl implements Runnable, ScreenshotEventProducer {

    private static final double TIME_TO_TAKE_SINGLE_IMAGE = 1000d / 5d;

    private final BlockingQueue<CsgoEvent> queue;
    private ScreenResolutionHelper resolutionHelper;
    private PressedKeysProvider pressedKeysProvider;
    private Robot robot;

    @Inject
    public ScreenshotEventProducerImpl(
            ScreenResolutionHelper resolutionHelper,
            Robot robot,
            PressedKeysProvider pressedKeysProvider,
            BlockingQueue<CsgoEvent> queue) {
        this.queue = queue;
        this.resolutionHelper = resolutionHelper;
        this.robot = robot;
        this.pressedKeysProvider = pressedKeysProvider;
    }

    @Override
    public void run() {
        while (true) {
            long start = System.currentTimeMillis();
            Set<Character> pressedKeys = pressedKeysProvider.getPressedKeys();
            int screenWidth = resolutionHelper.getWidth();
            int screenHeight = resolutionHelper.getHeight();
            Rectangle rect = new Rectangle(screenWidth, screenHeight);
            BufferedImage image = robot.createScreenCapture(rect);

            CsgoEvent event = new CsgoEvent(image, pressedKeys);
            queue.add(event);

            log.info("Queue size : " + queue.size());

            long stop = System.currentTimeMillis();
            long delta = stop - start;
            if (delta < TIME_TO_TAKE_SINGLE_IMAGE) {
                try {
                    Thread.sleep((long) (TIME_TO_TAKE_SINGLE_IMAGE - delta));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}