package org.bitbucket.arvade.csgoapi.common;

import java.awt.image.BufferedImage;

public interface ImageScaler {

    BufferedImage scaleImage(BufferedImage originalImage, int width, int height);

}
