package org.bitbucket.arvade.csgoapi.repository;

import org.bitbucket.arvade.csgoapi.model.RawCsgoEventResource;

import java.util.List;
import java.util.Optional;

public interface CsgoEventRepository{

    void save(List<RawCsgoEventResource> rawCsgoEventResourceList);

    Optional<RawCsgoEventResource> findById(Integer id);
}
