package org.bitbucket.arvade.csgoapi.repository.impl;

import com.google.inject.Inject;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.bitbucket.arvade.csgoapi.model.RawCsgoEventResource;
import org.bitbucket.arvade.csgoapi.repository.CsgoEventRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class CsgoEventRepositoryImpl implements CsgoEventRepository {

    private MysqlDataSource dataSource;

    @Inject
    public CsgoEventRepositoryImpl(MysqlDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void save(List<RawCsgoEventResource> rawCsgoEventResourceList) {
        try {
            saveInternal(rawCsgoEventResourceList);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void saveInternal(List<RawCsgoEventResource> rawCsgoEventResourceList) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement("INSERT INTO csgoevent (w_pressed, s_pressed,a_pressed,d_pressed,ctrl_pressed, space_pressed, image) VALUES (?,?,?,?,?,?,?)")) {

                for (RawCsgoEventResource entity : rawCsgoEventResourceList) {
                    appendRawCsgoEventResource(ps, entity);
                }

                ps.executeBatch();
            }
        }
    }

    private void appendRawCsgoEventResource(PreparedStatement ps, RawCsgoEventResource entity) throws SQLException {
        ps.setBoolean(1, entity.getWPressed());
        ps.setBoolean(2, entity.getSPressed());
        ps.setBoolean(3, entity.getAPressed());
        ps.setBoolean(4, entity.getDPressed());
        ps.setBoolean(5, entity.getCtrlPressed());
        ps.setBoolean(6, entity.getSpacePressed());
        ps.setBytes(7, entity.getImage());
        ps.addBatch();
    }

    public Optional<RawCsgoEventResource> findById(Integer id) {
        try {
            return findByIdInternal(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Optional<RawCsgoEventResource> findByIdInternal(Integer id) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM csgoevent WHERE id = ?")) {
                ps.setInt(1, id);
                RawCsgoEventResource result;
                try (ResultSet rs = ps.executeQuery()) {

                    if (!rs.next()) {
                        return Optional.empty();
                    }

                    result = mapResultSetToRawCsgoEventResource(rs);
                }
                return Optional.of(result);
            }
        }
    }

    private RawCsgoEventResource mapResultSetToRawCsgoEventResource(ResultSet resultSet) throws SQLException {
        RawCsgoEventResource resource = new RawCsgoEventResource();

        resource.setId((long) resultSet.getInt("id"));
        resource.setWPressed(resultSet.getBoolean("w_pressed"));
        resource.setWPressed(resultSet.getBoolean("s_pressed"));
        resource.setWPressed(resultSet.getBoolean("a_pressed"));
        resource.setWPressed(resultSet.getBoolean("d_pressed"));
        resource.setWPressed(resultSet.getBoolean("ctrl_pressed"));
        resource.setWPressed(resultSet.getBoolean("space_pressed"));
        resource.setImage(resultSet.getBytes("image"));

        return resource;
    }
}
