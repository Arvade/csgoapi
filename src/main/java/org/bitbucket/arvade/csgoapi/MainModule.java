package org.bitbucket.arvade.csgoapi;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.bitbucket.arvade.csgoapi.common.ImageScaler;
import org.bitbucket.arvade.csgoapi.common.impl.ImageScalerImpl;
import org.bitbucket.arvade.csgoapi.listener.PressedKeysProvider;
import org.bitbucket.arvade.csgoapi.listener.impl.PressedKeysProviderImpl;
import org.bitbucket.arvade.csgoapi.model.Configuration;
import org.bitbucket.arvade.csgoapi.model.CsgoEvent;
import org.bitbucket.arvade.csgoapi.model.EventConsumerConfiguration;
import org.bitbucket.arvade.csgoapi.provider.DatabaseConfigurationProvider;
import org.bitbucket.arvade.csgoapi.provider.SqlDataSourceProvider;
import org.bitbucket.arvade.csgoapi.repository.CsgoEventRepository;
import org.bitbucket.arvade.csgoapi.repository.impl.CsgoEventRepositoryImpl;
import org.bitbucket.arvade.csgoapi.service.CsgoEventConverter;
import org.bitbucket.arvade.csgoapi.service.ScreenshotEventProducer;
import org.bitbucket.arvade.csgoapi.service.impl.CsgoEventConverterImpl;
import org.bitbucket.arvade.csgoapi.service.impl.ScreenshotEventProducerImpl;
import org.bitbucket.arvade.csgoapi.util.ScreenResolutionHelper;
import org.bitbucket.arvade.csgoapi.util.impl.ScreenResolutionHelperImpl;
import org.yaml.snakeyaml.Yaml;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MainModule extends AbstractModule {

    protected void configure() {
        bind(CsgoEventRepository.class).to(CsgoEventRepositoryImpl.class).asEagerSingleton();
        bind(PressedKeysProvider.class).to(PressedKeysProviderImpl.class);
        bind(ScreenResolutionHelper.class).to(ScreenResolutionHelperImpl.class);
        bind(Yaml.class).toInstance(new Yaml());
        bind(Configuration.class).toProvider(DatabaseConfigurationProvider.class);
        bind(CsgoEventConverter.class).to(CsgoEventConverterImpl.class);
        bind(MysqlDataSource.class).toProvider(SqlDataSourceProvider.class);
        bind(ImageScaler.class).to(ImageScalerImpl.class);

        bind(EventConsumerConfiguration.class).toInstance(new EventConsumerConfiguration(50));
        bind(new TypeLiteral<BlockingQueue<CsgoEvent>>() {}).toInstance(new ArrayBlockingQueue<>(100));
        bind(ScreenshotEventProducer.class).to(ScreenshotEventProducerImpl.class);
    }
}
