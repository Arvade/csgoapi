package org.bitbucket.arvade.csgoapi.provider;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.bitbucket.arvade.csgoapi.model.Configuration;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DatabaseConfigurationProvider implements Provider<Configuration> {

    private static final String DATABASE_CONFIGURATION_FILE_NAME = "config.yml";

    private Yaml yaml;

    @Inject
    private DatabaseConfigurationProvider(Yaml yaml) {
        this.yaml = yaml;
    }

    @Override
    public Configuration get() {
        Configuration configuration = null;

        try (InputStream in = Files.newInputStream(Paths.get("/"+DATABASE_CONFIGURATION_FILE_NAME))) {
            configuration = yaml.loadAs(in, Configuration.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return configuration;
    }
}
