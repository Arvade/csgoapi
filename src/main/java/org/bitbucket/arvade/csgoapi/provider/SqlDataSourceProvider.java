package org.bitbucket.arvade.csgoapi.provider;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.bitbucket.arvade.csgoapi.model.Configuration;

import java.util.Map;

public class SqlDataSourceProvider implements Provider<MysqlDataSource> {

    private Provider<Configuration> databaseConfigurationProvider;

    @Inject
    public SqlDataSourceProvider(Provider<Configuration> databaseConfigurationProvider) {
        this.databaseConfigurationProvider = databaseConfigurationProvider;
    }


    @Override
    public MysqlDataSource get() {
        Configuration configuration = databaseConfigurationProvider.get();
        Map.Entry<String, String> credentials = configuration.getUsers().entrySet().iterator().next();

        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUser(credentials.getKey());
        dataSource.setPassword(credentials.getValue());
        dataSource.setUrl(configuration.getConnection().getUrl());
        return dataSource;
    }
}
