package org.bitbucket.arvade.csgoapi.util.impl;

import org.bitbucket.arvade.csgoapi.util.ScreenResolutionHelper;

import java.awt.*;

public class ScreenResolutionHelperImpl implements ScreenResolutionHelper {

    private GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();


    public int getWidth() {
        return graphicsEnvironment.getDefaultScreenDevice().getDisplayMode().getWidth();
    }

    public int getHeight() {
        return graphicsEnvironment.getDefaultScreenDevice().getDisplayMode().getHeight();
    }
}
