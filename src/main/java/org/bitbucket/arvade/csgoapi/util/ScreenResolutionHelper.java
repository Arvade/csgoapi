package org.bitbucket.arvade.csgoapi.util;

public interface ScreenResolutionHelper {

    int getWidth();

    int getHeight();
}
