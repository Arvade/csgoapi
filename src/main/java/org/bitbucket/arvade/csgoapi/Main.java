package org.bitbucket.arvade.csgoapi;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.bitbucket.arvade.csgoapi.service.ScreenshotEventProducer;
import org.bitbucket.arvade.csgoapi.service.impl.EventConsumer;

import java.awt.*;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        MainModule module = new MainModule();
        Injector injector = Guice.createInjector(module);


        ScreenshotEventProducer producer = injector.getInstance(ScreenshotEventProducer.class);

        for (int i = 0; i < 1; i++) {
            EventConsumer eventConsumer = injector.getInstance(EventConsumer.class);
            new Thread(eventConsumer).start();
        }

        producer.run();


/*
        CsgoEventRepository instance = injector.getInstance(CsgoEventRepository.class);
        Optional<RawCsgoEventResource> byId = instance.findById(3206);

        RawCsgoEventResource entity = byId.get();
        byte[] image = entity.getImage();

        RandomAccessFile randomAccessFile = new RandomAccessFile("example.jpg", "rw");
        randomAccessFile.write(image);
        randomAccessFile.close();*/
    }
}
