package org.bitbucket.arvade.csgoapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class EventConsumerConfiguration {

    private Integer sizeOfArrayToStoreEntities;
}
