package org.bitbucket.arvade.csgoapi.model;

import lombok.Data;

@Data
public class RawCsgoEventResource {

    private Long id;
    private Boolean wPressed;
    private Boolean sPressed;
    private Boolean aPressed;
    private Boolean dPressed;
    private Boolean ctrlPressed;
    private Boolean spacePressed;
    private byte[] image;
}