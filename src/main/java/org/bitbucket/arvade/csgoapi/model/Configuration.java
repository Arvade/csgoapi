package org.bitbucket.arvade.csgoapi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

@Getter
@Setter
public final class Configuration {

    private Date released;
    private String version;
    private Connection connection;
    private List<String> protocols;
    private Map<String, String> users;

    @Override
    public String toString() {
        return new StringBuilder()
                .append(format("Version: %s\n", version))
                .append(format("Released: %s\n", released))
                .append(format("Connecting to database: %s\n", connection))
                .append(format("Supported protocols: %s\n", protocols))
                .append(format("Users: %s\n", users))
                .toString();
    }
}