package org.bitbucket.arvade.csgoapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class Connection {

    private String url;
    private int poolSize;

    @Override
    public String toString() {
        return String.format("'%s' with pool of %d", getUrl(), getPoolSize());
    }
}
