package org.bitbucket.arvade.csgoapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.awt.image.BufferedImage;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
public class CsgoEvent {

    private Long id;

    private BufferedImage bufferedImage;

    private Set<Character> pressedKeys;

    public CsgoEvent(BufferedImage bufferedImage, Set<Character> pressedKeys) {
        this.bufferedImage = bufferedImage;
        this.pressedKeys = pressedKeys;
    }
}
