package org.bitbucket.arvade.csgoapi.listener.impl;

import org.bitbucket.arvade.csgoapi.listener.PressedKeysProvider;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.util.HashSet;
import java.util.Set;

public class PressedKeysProviderImpl implements NativeKeyListener, PressedKeysProvider {

    private Set<Character> pressedKeys = new HashSet<>();

    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
        Character keyCode = nativeKeyEvent.getKeyChar();
        pressedKeys.add(keyCode);
    }

    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
        System.out.println("pressed : " + nativeKeyEvent.getKeyChar());
        System.out.println("pressed : " + nativeKeyEvent.getKeyCode());

        Character keyCode = nativeKeyEvent.getKeyChar();
        if (pressedKeys.contains(keyCode)) {
            pressedKeys.remove(keyCode);
        } else {
            pressedKeys.add(keyCode);
        }
    }

    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
    }

    public Set<Character> getPressedKeys() {
        return this.pressedKeys;
    }
}
