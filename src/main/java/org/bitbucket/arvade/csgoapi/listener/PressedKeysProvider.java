package org.bitbucket.arvade.csgoapi.listener;

import java.util.Set;

public interface PressedKeysProvider {

    Set<Character> getPressedKeys();
}
